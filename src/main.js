import URD from './components/UploadResearchData.svelte';
import ERD from "./components/EditResearchData.svelte";
import Droparea from "./components/DropareaInternal.svelte";
import UploadResources from "./components/UploadResources.svelte";

class URDWrapper extends URD {
    static get observedAttributes() {
        return (super.observedAttributes || []).map(attr => attr.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase());
    }

    attributeChangedCallback(attrName, oldValue, newValue) {
        attrName = attrName.replace(/-([a-z])/g, (_, up) => up.toUpperCase());
        super.attributeChangedCallback(attrName, oldValue, newValue);
    }
}

class ERDWrapper extends ERD {
    static get observedAttributes() {
      return (super.observedAttributes || []).map(attr => attr.replace(/([a-zA-Z])(?=[A-Z])/g, '$1-').toLowerCase());
    }
  
    attributeChangedCallback(attrName, oldValue, newValue) {
      attrName = attrName.replace(/-([a-z])/g, (_, up) => up.toUpperCase());
      super.attributeChangedCallback(attrName, oldValue, newValue);
    }
}




customElements.define("drop-area",Droparea);
customElements.define("upload-resources", UploadResources);
customElements.define('upload-research-data', URDWrapper);
customElements.define("edit-research-data",ERDWrapper);
export {URDWrapper};
export {ERDWrapper};
export {Droparea};
export {UploadResources};
//export * from "./queryTools.js"
