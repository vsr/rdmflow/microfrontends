export function url_encode(url, params)
{
    let out = url + "?";
    for(let i = 0; i<params.length; i++)
    {
        out += params[i][0] + "=" + params[i][1] + "&";
    }
    return out;
}

export async function makeApiCall_CKAN(url,data=null, method="GET")
{
    url = `http://localhost:5000/${url}`;
    console.log(url);
    const API_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ1YjN6RTBNaE9hbVpKeE9zUmlFb0t2Z0dvX19zcTRrVjFuZWtWbmdUZDFzN3NtMHJLUDBTMi1scnluamdrZDJ4VGwzZ1JYWEhKeXJQd2RCaCIsImlhdCI6MTY2MDE0ODQ3MX0.couZDrx1CY-4ziei33srVPf1V73ewi5YdlcM64_5-18";
    //^-------This is just for debugging purposes for local. Call the custom access control
    const res = await fetch(url, {
        method: method,
        //cache: "no-cache",
        mode: "cors",
        headers:{
            "Content-Type": "application/json",
            "Authorization" : API_TOKEN
        },
        body: data == null?null:JSON.stringify(data) 
    });

    return await res.json();
}

export async function makeResourceCreateCall_CKAN(files, id)
{
    let url = "http://localhost:5000/api/action/resource_create";
    const API_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJqdGkiOiJ1YjN6RTBNaE9hbVpKeE9zUmlFb0t2Z0dvX19zcTRrVjFuZWtWbmdUZDFzN3NtMHJLUDBTMi1scnluamdrZDJ4VGwzZ1JYWEhKeXJQd2RCaCIsImlhdCI6MTY2MDE0ODQ3MX0.couZDrx1CY-4ziei33srVPf1V73ewi5YdlcM64_5-18";
    let promises = []

    for(let i = 0; i<files.length; i++)
    {
        let file = files[i];
        let formData = new FormData();
        formData.append("upload", file.file);
        
        formData.append("package_id",id);
        formData.append("description", file.descr);
        formData.append("name", file.title);
        
        promises.push( fetch(url, {
            method: "POST",
            //cache: "no-cache",
            mode: "cors",
            headers:{
                "Authorization" : API_TOKEN
            },
            body: formData
        }));
    };
    let p = await Promise.all(promises);

}